from django.contrib.auth import logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .models import User
import hashlib
# Create your views here.


def reg_view(request):
    # 注册
    # GET   返回页面
    if request.method == 'GET':
        return render(request, 'user/register.html')

    # POST  处理提交数据
    elif request.method == 'POST':

        username = request.POST['username']
        password_1 = request.POST['password_1']
        password_2 = request.POST['password_2']

    # 1、两个密码保持一致
        if password_1 != password_2:
            return HttpResponse('两次密码输入不一致')

    # 哈希算法 - 给定密文，计算的值不可逆
    #     定长输出
    #     不可逆
    #     雪崩效应
        m = hashlib.md5()
        # 此处需要字节串
        m.update(password_1.encode())
        password_m = m.hexdigest()

    # 2、当前用户名是否可用（unique唯一约束）
        old_users = User.objects.filter(username=username)
        if old_users:
            return HttpResponse('该用户名已注册')
    # 3、均满足插入数据【明文处理】
    #  请求量大下面写法会发生报错，存在并发写入问题，在含有 唯一索引 的地方一定要 try 对错误进行规避
    #     User.objects.create(username=username, password=password_m)
        try:
            user = User.objects.create(username=username, password=password_m)
        except Exception as e:
            print('--create user error %s'% e)
            return HttpResponse('用户名已经注册')

        # 免登录，使用session
        request.session['username'] = username
        request.session['uid'] = user.id

        # 对已经登陆或者注册的用户进行页面跳转到首页
        return HttpResponseRedirect('/index')


def login_view(request):

    if request.method == 'GET':

        # 判断该用户是否已经登录过该页面
        # 进行两步检查：一是直接检查session，二是检查cookies，如果有回写给session
        if request.session.get('username') and request.session.get('uid'):
            return HttpResponseRedirect('/index')

        c_username = request.COOKIES.get('username')
        c_uid = request.COOKIES.get('uid')
        if c_username and c_uid:
            request.session['username'] = c_username
            request.session['uid'] = c_uid

            return HttpResponseRedirect('/index')

        return render(request, 'user/login.html')

    elif request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        try:
            user = User.objects.get(username=username)
        except Exception as e:
            print('--login user error %s' % e)
            return HttpResponse('用户名或密码错误')

        # 对比密码
        m = hashlib.md5()
        m.update(password.encode())

        if m.hexdigest() != user.password:
            return HttpResponse('用户名或密码错误')

        # 记录会话信息
        request.session['username'] = username
        request.session['uid'] = user.id

        # 判断用户是否点选了“记住用户名”

        resp = HttpResponseRedirect('/index')

        if 'remember' in request.POST:
            resp.set_cookie('username', username, 3600*24*3)
            resp.set_cookie('uid', user.id, 3600*24*3)

        return resp


def logout_view(request):

    request.session.flush()
    logout(request)
    if 'username' in request.session:
        del request.session['username']
    if 'uid' in request.session:
        del request.session['uid']
    resp = HttpResponseRedirect('/index')
    if 'username' in request.COOKIES:
        resp.delete_cookie('username')
    if 'uid' in request.COOKIES:
        resp.delete_cookie('uid')

    return resp