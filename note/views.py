from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Note


# 装饰器可以用来在不改变原来函数构造的情况下完成对函数本身功能的拓展
# 状态校验
def verify(func):
    def wrapper(request, *args, **kwargs):
        if 'username' not in request.session or 'uid' not in request.session:
            if 'username' not in request.COOKIES or 'uid' not in request.COOKIES:
                return HttpResponseRedirect('/user/login')
            else:
                request.session['username'] = request.COOKIES.get('username')
                request.session['uid'] = request.COOKIES.get('uid')
        return func(request, *args, **kwargs)
    return wrapper


# Create your views here.
def list_view(request):
    if request.method == 'GET':
        note = Note.objects.values()
        # locals函数用来将所有参数传递到页面中用来显示
        # 这样写符合python的简约要求但是会将无用参数传递
        return render(request, 'note/list_note.html', locals())


@verify     # 使用装饰器验证用户是否登陆，如果已登陆可进行添加笔记，未登录则跳转到登陆页面
def add_note_view(request):
    if request.method == 'GET':
        return render(request, 'note/add_note.html')
    elif request.method == 'POST':
        title = request.POST.get('title')
        content = request.POST.get('content')
        # 这里面uid要从COOKIES中获取，因为此时POST提交中并未包含创作者信息
        uid = request.COOKIES.get('uid')
        Note.objects.create(title=title, content=content, user_id=uid)
        return HttpResponseRedirect('/note/note_list')


def update_note_view(request, p):
    if request.method == 'GET':
        notes = Note.objects.get(id=p)
        title = notes.title
        content = notes.content
        return render(request, 'note/update_note.html', locals())
    elif request.method == 'POST':
        newTitle = request.POST.get('title')
        newContent = request.POST.get('content')
        oldNote = Note.objects.get(id=p)
        oldNote.title = newTitle
        oldNote.content = newContent
        oldNote.save()
        return HttpResponseRedirect('/note/note_list')


def del_note_view(request, p):
    try:
        note = Note.objects.get(id=p)
        note.delete()
    except Exception as e:
        print('----delete note is fail: %s' % e)
    return HttpResponseRedirect('/note/note_list')
