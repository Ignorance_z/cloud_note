# -*- coding: utf-8 -*-
from django.urls import path

from . import views

urlpatterns = [
    path('note_list', views.list_view),
    path('add', views.add_note_view),
    path('update/<int:p>', views.update_note_view),
    path('delete/<int:p>', views.del_note_view)
]
