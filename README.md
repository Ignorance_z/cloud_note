# cloud_note云笔记项目实现

该仓库是对Django云笔记项目的实现，目的是为了练习gitee开发项目代码提交 

 **参考视频或博客**：

1）https://www.bilibili.com/video/BV1b5411c7Sa?p=31&vd_source=1b864a74dc4fe4d1cca2dc0cf70bd061

2）https://blog.csdn.net/togph/article/details/118518975


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
